<!DOCTYPE html>
<html  <?php language_attributes(); ?>  >
<head>
  <title>
    <?php echo get_bloginfo( 'name' ); ?>
  </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Mersa Oil" />
  <meta name="robots" content="all" />
  <meta name="googlebot" content="all" />
  <meta name="google-site-verification" content="..." />
  <?php wp_head();?>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body <?php body_class();  $url = home_url( '/' ); $lang=get_bloginfo("language");?>>
  <header id="header" class="container-fluid">
    <div class="row info">
      <div class="col-xs-12 text-right">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <?php if ( is_active_sidebar( 'ustadres' ) ) : ?>
                  <?php dynamic_sidebar( 'ustadres' ); ?>
                  <?php endif; ?>
                    <div class="phone"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/phone.svg"> +90 (212) 222 4466</div>
                    <div class="mail"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/mail.svg"> info@mersaoil.com</div>
                    <div class="language">
                    <a>
                      <?php  
                        pll_the_languages(
                          array('display_names_as'=> 'slug')
                        );
                      ?>
                    </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo esc_url( $url )?> ">
                      <img src="
                        <?php
                          $custom_logo_id = get_theme_mod( 'custom_logo' );
                          $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                          echo $image[0];
                        ?>">
                    </a>
                  </div>
              
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">

                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <?php 
                            if(pll_current_language()=="en"){
                              echo "CORPORATE";
                            }
                            if(pll_current_language()=="fr"){
                              echo "ENTREPISE";
                            }
                            if(pll_current_language()=="ar"){
                              echo "الشركات";
                            }
                          ?>
                        </a>
                        <div class="dropdown-menu">
                          <div class="container">
                            <div class="row">
                              <div class="col-md-4">
                                <div class="menu-description">
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/corporate-icon.svg">
                                  <h2>
                                    <?php 
                                      if(pll_current_language()=="en"){
                                        echo "CORPORATE";
                                      }
                                      if(pll_current_language()=="fr"){
                                        echo "ENTREPISE";
                                      }
                                      if(pll_current_language()=="ar"){
                                        echo "الشركات";
                                      }
                                    ?>
                                  </h2>
                                  <p>
                                    <?php 
                                      if(pll_current_language()=="en"){
                                        echo "Our mission is to gain a place in the lubricant industry; in the neighboring countries; in the Middle East and Africa market with high-class and competitive products by meeting the customer satisfaction.";
                                      }
                                      if(pll_current_language()=="fr"){
                                        echo "Notre mission est de gagner une place dans l'industrie des lubrifiants; dans les pays voisins; dans le Moyen-Orient et en Afrique marché avec des produits de haute classe et compétitifs en répondant à la satisfaction du client.";
                                      }
                                      if(pll_current_language()=="ar"){
                                        echo "مهمتنا هي الحصول على مكان في صناعة زيوت التشحيم؛ في البلدان المجاورة؛ في الشرق الأوسط وأفريقيا السوق مع الدرجة العالية و المنتجات التنافسية من خلال تلبية رضا العملاء.";
                                      }
                                    ?>
                                  </p>
                                </div>
                              </div>
    
                              <div class="col-md-4">
                                <div class="second-menu">
                                  <h3>
                                    <?php 
                                      if(pll_current_language()=="en"){
                                        echo "CORPORATE";
                                      }
                                      if(pll_current_language()=="fr"){
                                        echo "ENTREPISE";
                                      }
                                      if(pll_current_language()=="ar"){
                                        echo "الشركات";
                                      }
                                    ?>
                                  </h3>
                                  <ul>
                                    <?php
                                      wp_nav_menu(
                                        array (
                                          'container' => false,
                                            'theme_location' => 'corporateinner',
                                            'walker'         => new WPSE_33175_Simple_Walker,
                                            'items_wrap' => '%3$s',
                                            )
                                      );
                                    ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <?php 
                            if(pll_current_language()=="en"){
                              echo "PRODUCT";
                            }
                            if(pll_current_language()=="fr"){
                              echo "PRODUIT";
                            }

                            if(pll_current_language()=="ar"){
                              echo "المنتج";
                            }
                          ?>
                        </a>
                        <div class="dropdown-menu">
                          <div class="container">
                            <div class="row">
                              <div class="col-md-4">
                                <div class="menu-description">
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/product-icon.svg">
                                  <h2>  
                                    <?php 
                                      if(pll_current_language()=="en"){
                                        echo "PRODUCT";
                                      }
                                      if(pll_current_language()=="fr"){
                                        echo "PRODUIT";
                                      }
                                      if(pll_current_language()=="ar"){
                                        echo "المنتج";
                                      }
                                    ?>
                                  </h2>
                                  <p>
                                    <?php 
                                      if(pll_current_language()=="en"){
                                        echo "5W/40 is specially improved for turbo charged normal aspirated cars with full synthetic motor oils,high speed and high performance.";
                                      }
                                      if(pll_current_language()=="fr"){
                                        echo "5W / 40 est spécialement améliorée pour les voitures à aspiration normale turbocompressées avec des huiles moteur entièrement synthétiques, haute vitesse et hautes performances. ";
                                      }
                                      if(pll_current_language()=="ar"){
                                        echo "يتم تحسين 5W / 40 خصيصا ل توربو مشحونة العاديين يستنشق السيارات مع كامل زيوت المحركات الاصطناعية، عالية السرعة وعالية الأداء.";
                                      }
                                    ?>
                                  </p>
                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="second-menu">
                                  <h3>
                                    <?php 
                                      if(pll_current_language()=="en"){
                                        echo "PRODUCT CATEGORIES";
                                      }
                                      if(pll_current_language()=="fr"){
                                        echo "CATÉGORIES DE PRODUITS";
                                      }
                                      if(pll_current_language()=="ar"){
                                        echo "فئات المنتجات ";
                                      }
                                    ?>
                                  </h3>
                                  <ul>
                                    <?php
                                      global $post;
                                      $args = array( 'posts_per_page' => 1000 );
                                      $myposts = get_posts( $args );
                                      $mycategories = [];
                                      foreach ( $myposts as $post ) : 
                                        setup_postdata( $post );
                                        if (get_the_category( $post->ID )[0]->name == "PRODUCT" || get_the_category( $post->ID )[0]->name == "Genel") {} else {
                                          array_push($mycategories, get_the_category( $post->ID )[0]->name);
                                        }
                                      endforeach;
                                      $new_categories = array_unique($mycategories);
                                      foreach ($new_categories as $category) :
                                        ?>
                                          <li><a href="#"><span class="icon"></span><?php echo $category; ?></a></li>
                                        <?php
                                      endforeach;
                                      wp_reset_postdata();
                                    ?>
                                  </ul>
                                </div>
                              </div>

                              <?php
                                global $post;
                                $args = array( 'posts_per_page' => 1000 );
                                $myposts = get_posts( $args );
                                $mycategories = [];
                                foreach ( $myposts as $post ) : 
                                  setup_postdata( $post );
                                  if (get_the_category( $post->ID )[0]->name == "PRODUCT" || get_the_category( $post->ID )[0]->name == "CORPORATE") {} else {
                                    array_push($mycategories, get_the_category( $post->ID )[0]->name);
                                  }
                                endforeach;
                                $new_categories = array_unique($mycategories);
                                foreach ($new_categories as $category) :
                                  ?>

                                    <div class="col-md-4 hidden">
                                      <div class="third-menu">
                                        <h3><?php echo $category; ?></h3>
                                        <ul>

                                          <?php
                                            foreach ( $myposts as $post ) : 
                                              if ($category == get_the_category( $post->ID )[0]->name) {
                                                ?>
                                                  <li><a href="<?php echo $post->guid ?>"><span class="icon"></span><?php echo $post->post_title ?></a></li>
                                                <?php
                                              }
                                            endforeach;
                                          ?>

                                        </ul>
                                      </div>
                                    </div>

                                  <?php
                                endforeach;
                                wp_reset_postdata();
                              ?>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php
                        wp_nav_menu(
                          array (
                            'container' => false,
                              'theme_location' => 'altsec',
                              'walker'         => new altsec,
                              'items_wrap' => '%3$s',

                              )
                          );
                      ?>
                    </ul>
                  </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div id="overlay"></div>

  <div id="loading">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/logo.svg">
    <div class="spinner">
      <div class="dot1"></div>
      <div class="dot2"></div>
    </div>
  </div>