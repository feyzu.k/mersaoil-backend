<?php 
/*
Template Name: SERVICES
Template Post Type:page
*/
// Page code here...
?>
<?php get_header(); ?>

<div id="corporate" class="container-fluid">
  <div class="row header">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
           <h1>SERVICES</h1>
           <p>
           
           
  
           
           
           
           
           
           </p>
          </div>
        </div>
      </div>
    </div>
  </div>


<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
  <div class="row content">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-md-4">
            <img src="<?php echo $url ?>">
          </div>
          <div class="col-sm-8 col-md-6">
          <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
			

			endwhile; // End of the loop.
			?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>









