<?php 
/*
Template Name: PARTNERS
Template Post Type:page
*/
// Page code here...
?>

<?php get_header(); ?>





<div id="partners" class="container-fluid">
  <div class="row header">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
           <h1>PARTNERS</h1>
           <p>Lorem ipsum dolor sit amet, quidam verear definiebas in sea, pro an quot propriae scaevola. Assentior definitionem cu mei, commodo perpetua at mei.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row content">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
        <div class="col-sm-4 col-md-8 col-md-offset-2 box">



       <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
			

			endwhile; // End of the loop.
			?>



</div>

   
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>

















