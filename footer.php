<footer class="container-fluid" id="footer">
  <div class="row social-band">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="content">
              <div class="box">
                <a href="#">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/facebook.png">
                  <p>Facebook <span>Follow Us</span></p>
                </a>
              </div>
              <div class="box">
                <a href="#">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/twitter.png">
                  <p>Twitter <span>Follow Us</span></p>
                </a>
              </div>
              <div class="box">
                <a href="#">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/google.png">
                  <p>Google + <span>Follow Us</span></p>
                </a>
              </div>
              <div class="box right">
                <a href="#">
                  <p>Click for <span>Online Catalog</span></p>
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/katalog.png">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row footer">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 col-xs-12 logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/white-logo.png">
          </div>
          <div class="col-sm-6 col-xs-12 info">
            <p>Mersa Kimya Mersa Depolama Kim. Pet. Tüt. ve Tar. Ürn. Teks. San. Tic. ve Ltd. Şti.
            
            <?php if ( is_active_sidebar( 'altadres' ) ) : ?>

    <?php dynamic_sidebar( 'altadres' ); ?>
    <?php endif; ?>
            
            
            </p>
            <span style="    font-family: proximaregular,serif;
    font-weight: 500;
    font-size: 12px;
    line-height: 17px;
    color: #fff;">Copyright © 2015 Mersa Oil. All Right Reserved.</span>

          </div>
          <div class="col-sm-3 col-xs-12 creative">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/creative-ad-works-logo.png">
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCU7JxuP7ErFaYF2ihdCG4tsmGKS2snHmM"></script>
</body>
</html>
