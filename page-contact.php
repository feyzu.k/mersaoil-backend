<?php 
/*
Template Name: CONTACT
Template Post Type:page
*/
// Page code here...
?>
<?php get_header(); ?>



<div id="contact" class="container-fluid">
  <div class="row header">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
           <h1>CONTACT</h1>
           <p>Lorem ipsum dolor sit amet, quidam verear definiebas in sea, pro an quot propriae scaevola. Assentior definitionem cu mei, commodo perpetua at mei.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row content">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-4 contact">
            <h2>HEAD OFFICE</h2>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/konum.svg">
              <span>Sütlüce Mh., İmrahor Cd No:36, Beyoğlu, İstanbul / Türkiye</span>
            </p>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/phone-black.svg">
              <span>+90 (212) 222 4466</span>
            </p>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/fax.svg">
              <span>+90 (212) 222 5044</span>
            </p>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/mail-black.svg">
              <span>info@mersaoil.com</span>
            </p>
            <h2>FACTORY</h2>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/konum.svg">
              <span>Serbest Bölge B7 Cad. DD Ada 5.Parsel Akdeniz/Mersin 33020</span>
            </p>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/phone-black.svg">
              <span>+90 (324) 233 0388</span>
            </p>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/fax.svg">
              <span>+90 (324) 233 0399</span>
            </p>
            <p>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/mail-black.svg">
              <span>info@mersaoil.com</span>
            </p>
          </div>
          <div class="col-md-8 form">
          <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
			

			endwhile; // End of the loop.
			?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row content">
      <div id="googleMap"></div>
  </div>
</div>







<?php get_footer(); ?>
