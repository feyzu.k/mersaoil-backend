<?php get_header(); ?>

<div id="home" class="container-fluid">
  <div class="row header">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-7 content">
            <h1><?php 
                      if(pll_current_language()=="en"){

                        echo "ALWAYS";
                      }
                      if(pll_current_language()=="fr"){

                        echo "TOUJOURS";
                      }

                      if(pll_current_language()=="ar"){

                        echo "دائما";
                      }
                      
                      ?></h1>
            <h2><?php 
                      if(pll_current_language()=="en"){

                        echo "READY TO MOVE";
                      }
                      if(pll_current_language()=="fr"){

                        echo "PRÊT À DÉPLACER";
                      }

                      if(pll_current_language()=="ar"){

                        echo "جاهز للتحرك";
                      }
                      
                      ?></h2>
            <p>
            
            <?php 
                      if(pll_current_language()=="en"){

                        echo "5W/40 is specially improved for turbo charged normal aspirated cars with full synthetic motor oils,high speed and high performance.";
                      }
                      if(pll_current_language()=="fr"){

                        echo "5W / 40 est spécialement améliorée pour les voitures à aspiration normale turbocompressées avec des huiles moteur entièrement synthétiques, haute vitesse et hautes performances. ";
                      }

                      if(pll_current_language()=="ar"){

                        echo "يتم تحسين 5W / 40 خصيصا ل توربو مشحونة العاديين يستنشق السيارات مع كامل زيوت المحركات الاصطناعية، عالية السرعة وعالية الأداء.";
                      }
                      
                      ?>
            
            
            
            
            
            
  </p>
            <a href="#">
            
            
            <?php 
                      if(pll_current_language()=="en"){

                        echo "SEE MORE";
                      }
                      if(pll_current_language()=="fr"){

                        echo "VOIR PLUS";
                      }

                      if(pll_current_language()=="ar"){

                        echo "شاهد المزيد";
                      }
                      
                      ?>
            
                        
            
            
        
            
            
            
            
             <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-5 hidden-sm">
            <img class="header-oil" src="<?php echo get_template_directory_uri(); ?>/assets/header-oil.png">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row categories">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
            <h3>
            
            
            
                        
            <?php 
                      if(pll_current_language()=="en"){

                        echo "ALL PRODUCT CATEGORIES";
                      }
                      if(pll_current_language()=="fr"){

                        echo "TOUTES LES CATÉGORIES DE PRODUITS";
                      }

                      if(pll_current_language()=="ar"){

                        echo "جميع فئات المنتجات";
                      }
                      
                      ?>
            
            
            
            
            
            
            
            
             <i class="fa fa-tint" aria-hidden="true"></i></h3>
            <div class="box">
              <div class="overlay"></div>
              <a href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/oil.png">
                <p class="categori-name">Gasoline Motor Oil</p>
                <span>CLICK FOR SEE PRODUCTS</span>
              </a>
            </div>
            <div class="box">
              <div class="overlay"></div>
              <a href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/oil.png">
                <p class="categori-name">Diesel Oil</p>
                <span>CLICK FOR SEE PRODUCTS</span>
              </a>
            </div>
            <div class="box">
              <div class="overlay"></div>
              <a href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/oil.png">
                <p class="categori-name">Motorcycle Oil</p>
                <span>CLICK FOR SEE PRODUCTS</span>
              </a>
            </div>
            <div class="box">
              <div class="overlay"></div>
              <a href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/oil.png">
                <p class="categori-name">Industry Oil</p>
                <span>CLICK FOR SEE PRODUCTS</span>
              </a>
            </div>
            <div class="box">
              <div class="overlay"></div>
              <a href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/oil.png">
                <p class="categori-name">Transmission Oil</p>
                <span>CLICK FOR SEE PRODUCTS</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row news">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
            <h3>NEWS</h3>

<?php
global $post;
$args = array( 'posts_per_page' => 4, 'orderby' => 'date',	'post_type' => 'haberler',);
$postslist = get_posts( $args );

foreach ( $postslist as $post ) :
  setup_postdata( $post ); ?> 

            <div class="box">
              <h4 class="header"><?php the_title(); ?></h4>
              <p class="date"><?php echo date_i18n( get_option( 'date_format' ), strtotime( $post->post_date ) ); ?></p>
              <p class="description"><?php  echo substr($post->post_content,0,145)."..."; ?></p>
              <a href="<?php  echo  $post->guid;?>">READ MORE <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>
<?php
endforeach; 
wp_reset_postdata();
?>






          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row partners">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
            <h3>OUR PARTNER</h3>
            <div class="box"><img src="<?php echo get_template_directory_uri(); ?>/assets/partner.jpg"></div>
            <div class="box"><img src="<?php echo get_template_directory_uri(); ?>/assets/partner.jpg"></div>
            <div class="box"><img src="<?php echo get_template_directory_uri(); ?>/assets/partner.jpg"></div>
            <div class="box"><img src="<?php echo get_template_directory_uri(); ?>/assets/partner.jpg"></div>
            <div class="box"><img src="<?php echo get_template_directory_uri(); ?>/assets/partner.jpg"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php get_footer();
