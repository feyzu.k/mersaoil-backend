<?php 
/*
Template Name: PRODUCT
Template Post Type:post
*/
// Page code here...
?>
<?php get_header(); ?>






<div id="product" class="container-fluid">
  <div class="row header">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
           <h1>PRODUCT</h1>
           <p><?php 

$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); 
$meta = get_post_meta( get_the_ID()); 
$yaz = array_values($meta);
if( array_search('1', $yaz[3])!==NULL  ){
  echo " 1 LT var <br>";
}
if( array_search('4', $yaz[3])!==NULL  ){
  echo " 4 LT var <br>";
}
if( array_search('5', $yaz[3])!==NULL  ){
  echo " 5 LT var <br>";
}
if( array_search('7', $yaz[3])!==NULL  ){
  echo " 7 LT var <br>";
}
if( array_search('208', $yaz[3])!==NULL  ){
  echo " 208 LT var <br>";
}

           ?>Lorem ipsum dolor sit amet, quidam verear definiebas in sea, pro an quot propriae scaevola. Assentior definitionem cu mei, commodo perpetua at mei.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row content">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-3 menu">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                     





                              <?php
                                global $post;
                                $args = array( 'posts_per_page' => 1000 );
                                $myposts = get_posts( $args );
                                $mycategories = [];


function replaceSpace($string)
{
  $string = preg_replace("/\s+/", "", $string);
  $string = trim($string);
    $string =strtolower(  $string);
  return $string;
}





                                foreach ( $myposts as $post ) : 
                                  setup_postdata( $post );
                                  if (get_the_category( $post->ID )[0]->name == "PRODUCT" || get_the_category( $post->ID )[0]->name == "CORPORATE") {} else {
                                    array_push($mycategories, get_the_category( $post->ID )[0]->name);
                                  }
                                endforeach;
                                $new_categories = array_unique($mycategories);
                                foreach ($new_categories as $category) :
                                  ?>

                                       <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php print replaceSpace($category); ?>" aria-expanded="true" aria-controls="<?php print replaceSpace($category); ?>">
                                            <?php echo $category; ?>             
                                                   </a>
                                            </h4>  
                                          </div>
                                <div id="<?php print replaceSpace($category); ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php print replaceSpace($category); ?>">
                                            <div class="panel-body">
                                              <ul>
                                                    
                                                  <?php
                                                    foreach ( $myposts as $post ) : 
                                                      if ($category == get_the_category( $post->ID )[0]->name) {
                                                        ?>
                                                          <li><a href="<?php echo $post->guid ?>"><span class="icon"></span><?php echo $post->post_title ?></a></li>
                                                        <?php
                                                      }
                                                    endforeach;
                                                  ?>


                                                  </ul>
                                            </div>
                                          </div>
                                        </div>


                                  <?php
                                endforeach;
                                wp_reset_postdata();
                              ?>







            </div>
          </div>
          <div class="col-md-6 description">
            <h2>
            <?php single_post_title(); ?>             
            <span>
           <?php
            $category = get_the_category();
            echo $category[0]->cat_name;
            ?>      
            </span>
            </h2>
       <?php
      while ( have_posts() ) : the_post();
      get_template_part( 'template-parts/page/content', 'page' );
      endwhile; // End of the loop.
      ?>
          </div>
          <div class="col-md-3 image">
            <img src="<?php echo $url ?>">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
