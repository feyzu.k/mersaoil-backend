<?php 
/*
Template Name: CORPARATE
Template Post Type:post
*/
// Page code here...
?>
<?php get_header(); ?>

<div id="corporate" class="container-fluid">
  <div class="row header">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12 content">
           <h1> <?php 
                      if(pll_current_language()=="en"){

                        echo "CORPORATE";
                      }
                      if(pll_current_language()=="fr"){

                        echo "ENTREPISE";
                      }

                      if(pll_current_language()=="ar"){

                        echo "الشركات";
                      }
                      
                      ?></h1>
           <p>
           
           
           <?php 
                      if(pll_current_language()=="en"){

                        echo "Our mission is to gain a place in the lubricant industry; in the neighboring countries; in the Middle East and Africa market with high-class and competitive products by meeting the customer satisfaction.";
                      }
                      if(pll_current_language()=="fr"){

                        echo "Notre mission est de gagner une place dans l'industrie des lubrifiants; dans les pays voisins; dans le Moyen-Orient et en Afrique marché avec des produits de haute classe et compétitifs en répondant à la satisfaction du client.";
                      }

                      if(pll_current_language()=="ar"){

                        echo "مهمتنا هي الحصول على مكان في صناعة زيوت التشحيم؛ في البلدان المجاورة؛ في الشرق الأوسط وأفريقيا السوق مع الدرجة العالية و المنتجات التنافسية من خلال تلبية رضا العملاء.";
                      }
                      
                      ?>
           
           
           
           
           
           
           
           
           
           </p>
          </div>
        </div>
      </div>
    </div>
  </div>


<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
  <div class="row content">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-md-4">
            <img src="<?php echo $url ?>">
          </div>
          <div class="col-sm-8 col-md-6">
          <?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
			

			endwhile; // End of the loop.
			?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>









